# README #

### RAM ###

* This is a repository for RAMSES Animation Maker
* Current version: 1.11 (December 2016)
* Wiki with examples is here: (https://bitbucket.org/biernacki/ram/wiki/Home)

RAM relies on [ffmpeg](https://ffmpeg.org) to produce movies, please adjust your bin_ffmpeg path in `ram.py`

From version 1.11 RAM requres `f90nml` (https://github.com/marshallward/f90nml.git), which can be installed with `pip install f90nml`.


Please, feel free to create issues and request new features!
