"""Handles some basic objects for RAMSES<->python interaction"""
import sys
import os
import glob
import warnings
import numpy
import f90nml
import fortranfile

# defining constants
# http://www.astro.wisc.edu/~dolan/constants.html
C = {'c': 2.99792458e10,  # cm/s
     'm_H': 1.6733e-24,  # g
     'k_B': 1.380658e-16,  # erg/K
     'Lambda_0': 1.2e-27,  # erg/s cm^3/K^0.5
     'G': 6.67259e-8,  # cm^3/gs^2
     'Msun': 1.989e33,  # g
     'sig_thompson': 6.652e-25,  # in cgs
     'kpc2cm': 3.086e21,
     'pc2cm': 3.086e18,
     'epsilon_r': 0.1,
     'epsilon_c': 0.15,
     's_in_yr': 86400.*365.25,
     'eV2erg': 1.6021766e-12,
     'a_rad': 7.5657e-15}


class Units(object):
    """This class handles basic units"""
    def __init__(self, unit_l, unit_d, unit_t):
        self.l = unit_l  # [cm]
        self.d = unit_d  # [g/cc]
        self.t = unit_t  # [s]
        self.m = unit_d*unit_l**3  # [g]
        self.v = unit_l/unit_t  # [cm/s]

        return

    def inkpc(self):
        """Returns lenght in kpc"""
        return self.l*3.24075e-19/1000.  # [kpc]

    def inMyr(self):
        """Returns time in Myr"""
        return self.t/(1e6*C['s_in_yr'])  # [Myr]

    def inMsun(self):
        """Returns mass in Msun"""
        return self.m/C['Msun']  # [Msun]
    def inkms(self):
        """Returns lenght in km/s"""
        return self.v / 1e5  # [km/s]


class Sink(object):  # class to overload with the sink data
    """Holds sinks"""
    def __init__(self, sid, mass, x, y, z, vx, vy, vz, acc_rate,
                 age=-1, rho_gas=-1, c2=-1, eps=-1, vgx=0, vgy=0, vgz=0):
        self.id = sid
        self.mass = mass
        self.x = x
        self.y = y
        self.z = z
        self.vx = vx
        self.vy = vy
        self.vz = vz
        self.pos = [self.x, self.y, self.z]
        self.age = age
        self.acc_rate = acc_rate
        self.rho_gas = rho_gas
        self.c2 = c2
        self.eps = eps
        self.vgx = vgx
        self.vgy = vgy
        self.vgz = vgz
        # Edd rate is in cu.mass/second
        self.edd = (self.mass*4.*numpy.pi*C['G']*C['m_H'] /
                    (C['epsilon_r']*C['sig_thompson']*C['c']))

        self.vrel = ((self.vgx-self.vx)**2 +
                     (self.vgy-self.vy)**2 +
                     (self.vgz-self.vz)**2)**0.5
        # BOOSTED v_Bondi in cu.velocity
        self.vbondi = (self.c2+self.vrel**2)**0.5
        # self.vbondi = self.c2**0.5
        # r_Bondi in cu.mass/cu.velocity**2
        # self.rbondi = C['G']*self.mass/self.vbondi**2
        self.rbondi = C['G']*self.mass/self.c2

        return


class Clump(object):  # class to overload PHEW clump data
    """Holds PHEW clumps"""
    def __init__(self, *args):
        if numpy.isnan(args[0][0]):
            args[0][0:4] = [-1, -1, -1, -1]
        self.index = int(args[0][0])
        self.level = int(args[0][1])
        self.parent = int(args[0][2])
        self.ncell = int(args[0][3])
        self.peak_x = args[0][4]
        self.peak_y = args[0][5]
        self.peak_z = args[0][6]
        self.peak_pos = [self.peak_x, self.peak_y, self.peak_z]
        self.rho_min = args[0][7]
        self.rho_plus = args[0][8]
        self.rho_av = args[0][9]
        self.mass = args[0][10]
        self.relevance = args[0][11]

        return


class Halo(object):  # class to overload PHEW halo data
    """Holds PHEW halo"""
    def __init__(self, *args):
        if numpy.isnan(args[0][0]):
            args[0][0:2] = [-1, -1]
        self.index = int(args[0][0])
        self.ncell = int(args[0][1])
        self.peak_x = args[0][2]
        self.peak_y = args[0][3]
        self.peak_z = args[0][4]
        self.peak_pos = [self.peak_x, self.peak_y, self.peak_z]
        self.rho_plus = args[0][5]
        self.mass = args[0][6]

        return


def load_map(args, k, idx, map_kind=''):
    """Load a fortran binary 2D map/movie RAMSES output"""
    # define map path
    if map_kind == '':
        map_file = "%s/movie%d/%s_%05d.map" % (args.dir, int(args.proj[k]),
                                               args.kind[k], idx)
    else:
        map_file = "%s/movie%d/%s_%05d.map" % (args.dir, int(args.proj[k]),
                                               map_kind, idx)
    if not os.path.exists(map_file):
        return []

    # read image data
    ffile = fortranfile.FortranFile(map_file)
    [t, dx, dy, dz] = ffile.readReals('d')
    [nx, ny] = ffile.readInts()
    dat = ffile.readReals()
    ffile.close()

    return dat


def load_sink(directory, idx, boxlen, movie=True):
    """Load sinks"""
    # defnining sink path
    if movie:
        sink_file = "%s/movie1/sink_%05d.txt" % (directory, idx)
    else:
        sink_file = "%s/output_%05d/sink_%05d.csv" % (directory, idx, idx)
    x, y, z = [0., 0., 0.]
    try:
        with warnings.catch_warnings():  # load sink id, mass and position
            warnings.simplefilter("ignore")
            # checking number of columns to determine the version
            with file(sink_file) as file_object, warnings.catch_warnings():
                ncol = len(file_object.readline().split())
            if ncol == 19:
                (sid, mass,
                 x, y, z,
                 vx, vy, vz,
                 age, acc_rate,
                 rho_gas, c2, eps,
                 vgx, vgy, vgz
                 ) = numpy.loadtxt(sink_file, delimiter=',',
                                   usecols=[b for b in range(0, 19)
                                            if b not in range(8, 11)],
                                   unpack=True)
            elif ncol == 12 or ncol == 13:  # legacy
                (sid, mass,
                 x, y, z,
                 vx, vy, vz,
                 acc_rate
                 ) = numpy.loadtxt(sink_file, delimiter=',',
                                   usecols=[b for b in range(0, 12)
                                            if b not in range(8, 11)],
                                   unpack=True)
            else:
                sinks = None

            x /= boxlen
            y /= boxlen
            z /= boxlen

            if ncol == 19:
                if isinstance(sid, numpy.float64):  # only one sink
                    sinks = [Sink(sid, mass, x, y, z, vx, vy, vz, acc_rate,
                                  age, rho_gas, c2, eps, vgx, vgy, vgz)]
                else:
                    sinks = [Sink(sid[n], mass[n], x[n], y[n], z[n],
                                  vx[n], vy[n], vz[n], acc_rate[n],
                                  age[n], rho_gas[n], c2[n], eps[n],
                                  vgx[n], vgy[n], vgz[n])
                             for n in xrange(len(sid))]
            if ncol == 12 or ncol == 13:
                if isinstance(sid, numpy.float64):  # only one sink
                    sinks = [Sink(sid, mass, x, y, z,
                                  vx, vy, vz, acc_rate)]
                else:
                    sinks = [Sink(sid[n], mass[n], x[n], y[n], z[n],
                                  vx[n], vy[n], vz[n], acc_rate[n])
                             for n in xrange(len(sid))]

    except ValueError:  # catch if no sinks exist (for sink creation)
        sinks = None
    except IOError:  # sink file missing
        sinks = None

    return sinks


def load_phew(directory, idx, what='halo'):
    """Loads PHEW output -- halo or clump
    Keyword arguments:
        directory (str) -- path to directory in which output is (default: )
        idx (int) -- output number (default: )
        what (string) --  'clump' or 'halo' (default: halo)

    Returns:
        object (Clump or Halo object) -- returns desired object loaded with
            PHEW data
    """
    # defnining halos path
    objects = []
    object_files = glob.glob(directory +
                             'output_%05i/%s_%05i.txt?????' % (idx, what, idx))

    if what == 'halo':
        obj = Halo
        elems = 7
    elif what == 'clump':
        obj = Clump
        elems = 12
    else:
        raise TypeError('Wrong kind of file! Use "halo" or "clump"')
    hnum = 0
    for object_file in object_files:
        linecount = 0
        with open(object_file) as inp:
            for line in inp:
                if linecount > 0:
                    line = line.strip('\n')
                    line = [float(x) for x in line.split()]
                    objects.append(obj([float(b) for b in line]))
                    hnum += 1
                linecount += 1

    if hnum == 0:  # empty files, but exist
        objects = obj([numpy.nan for b in xrange(elems)])

    return objects


def load_times(directory, i, cosmo=False):
    """Load times of frames"""
    if i > 0:
        info_file = "%s/movie1/info_%05d.txt" % (directory, i)
    else:
        info_file = directory
    if not os.path.exists(info_file):
        return None
    infof = open(info_file)
    for j, line in enumerate(infof):
        if cosmo:
            if j == 9:  # instead of t we get the aexp
                time = float(line.split()[2])
        else:
            if j == 8:
                time = float(line.split()[2])
        if j > 9:
            break
    infof.close()

    return time


def load_namelist(nml_path='', directory=''):
    """Loads RAMSES namelist info"""
    if nml_path == '':
        nml_path = sorted(glob.glob(directory+'output_0????/namelist.txt'))[-1]

    try:
        with open(nml_path) as nml_file:
            nml = f90nml.read(nml_file)
    except IOError:
        print "No namelist found! Aborting!"
        sys.exit()

    # check if sim is cosmological
    try:
        nml['run_params']['cosmo']
    except KeyError:
        nml['run_params']['cosmo'] = False

    if nml['run_params']['cosmo']:
        nml['amr_params']['boxlen'] = 1.

    # check if there are sinks
    try:
        nml['run_params']['sink']
    except KeyError:
        nml['run_params']['sink'] = False

    # check if sim is with RT
    try:
        nml['run_params']['rt']
    except KeyError:
        nml['run_params']['rt'] = False

    # set some movie defaults
    if nml['run_params']['cosmo']:
        try:
            nml['movie_params']['astartmov']
        except KeyError:
            nml['movie_params']['astartmov'] = 0.
        try:
            nml['movie_params']['aendmov']
        except KeyError:
            nml['movie_params']['aendmov'] = nml['output_params']['aend']
        try:
            nml['movie_params']['tend_theta_camera']
        except KeyError:
            nml['movie_params']['tend_theta_camera'] = [nml['movie_params']['aendmov'] for i in xrange(5)]
        try:
            nml['movie_params']['tend_phi_camera']
        except KeyError:
            nml['movie_params']['tend_phi_camera'] = [nml['movie_params']['aendmov'] for i in xrange(5)]
    else:
        try:
            nml['movie_params']['tstartmov']
        except KeyError:
            nml['movie_params']['tstartmov'] = 0.
        try:
            nml['movie_params']['tendmov']
        except KeyError:
            nml['movie_params']['tendmov'] = nml['output_params']['tend']
        try:
            nml['movie_params']['tend_theta_camera']
        except KeyError:
            nml['movie_params']['tend_theta_camera'] = [nml['movie_params']['tendmov'] for i in xrange(5)]
        try:
            nml['movie_params']['tend_phi_camera']
        except KeyError:
            nml['movie_params']['tend_phi_camera'] = [nml['movie_params']['tendmov'] for i in xrange(5)]

    try:
        nml['movie_params']['tstart_theta_camera']
    except KeyError:
        nml['movie_params']['tstart_theta_camera'] = [0. for i in xrange(5)]
    try:
        nml['movie_params']['tstart_phi_camera']
    except KeyError:
        nml['movie_params']['tstart_phi_camera'] = [0. for i in xrange(5)]

    try:
        nml['movie_params']['theta_camera']
    except KeyError:
        nml['movie_params']['theta_camera'] = [0. for i in xrange(5)]
    try:
        nml['movie_params']['dtheta_camera']
    except KeyError:
        nml['movie_params']['dtheta_camera'] = [0. for i in xrange(5)]

    try:
        nml['movie_params']['phi_camera']
    except KeyError:
        nml['movie_params']['phi_camera'] = [0. for i in xrange(5)]
    try:
        nml['movie_params']['dphi_camera']
    except KeyError:
        nml['movie_params']['dphi_camera'] = [0. for i in xrange(5)]

    try:
        nml['movie_params']['dist_camera']
    except KeyError:
        nml['movie_params']['dist_camera'] = [0. for i in xrange(5)]
    nml['movie_params']['dist_camera'] = [nml['amr_params']['boxlen'] for i in nml['movie_params']['dist_camera'] if i <= 0.]

    try:
        nml['movie_params']['ddist_camera']
    except KeyError:
        nml['movie_params']['ddist_camera'] = [0. for i in xrange(5)]

    try:
        nml['movie_params']['focal_camera']
    except KeyError:
        nml['movie_params']['focal_camera'] = [0. for i in xrange(5)]
    try:
        nml['movie_params']['perspective_camera']
    except KeyError:
        nml['movie_params']['perspective_camera'] = [False for i in xrange(5)]

    return nml


def load_units(directory, idx=-1, proj=0):
    """Loads units from RAMSES info file"""
    if idx > -1:  # loads from movie dir
        info_file = '{dir}/movie{proj:1d}/info_{num:05d}.txt'.\
                    format(dir=directory, proj=proj, num=idx)
    else:  # loads from full output
        info_file = glob.glob('{dir}/info_0????.txt'.
                              format(dir=directory))[0]
    try:
        infof = open(info_file)
    except IOError:
        sys.exit('Units file not found. Exiting!')

    for j, line in enumerate(infof):
        if j == 15:
            unit_l = float(line.split()[2])
        if j == 16:
            unit_d = float(line.split()[2])
        if j == 17:
            unit_t = float(line.split()[2])
        if j > 18:
            break
    infof.close()

    units = Units(unit_l, unit_d, unit_t)
    return units


def load_info_rt(directory, idx=-1, proj=0):
    """Loads RAMSES RT-related quantities"""
    if idx > -1:  # loads from movie dir
        info_file = '{dir}/movie{proj:1d}/info_rt_{num:05d}.txt'.\
                    format(dir=directory, proj=proj, num=idx)
    else:  # loads from full output
        info_file = glob.glob('{dir}/info_rt_0????.txt'.
                              format(dir=directory))[0]
    try:
        infof = open(info_file)
    except IOError:
        sys.exit('RT info file not found. Exiting!')

    groupE = []

    # Loading parameters from the namelist
    for i, line in enumerate(infof):
        if line.split('=')[0].strip() == 'nGroups':
            nGroups = int(line.split('=')[1])
        if line.split('=')[0].strip()[:3] == 'egy':
            groupE.append(float(line.split('=')[1]))

    return groupE[0:nGroups]
